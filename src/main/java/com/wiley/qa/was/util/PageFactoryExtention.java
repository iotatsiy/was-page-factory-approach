package com.wiley.qa.was.util;

import com.google.common.base.Predicate;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.sbtqa.tag.pagefactory.PageFactory;


public class PageFactoryExtention {

    public static WebDriverWait getWebDriverWait() {
        return new WebDriverWait(PageFactory.getDriver(), PageFactory.getTimeOut());
    }

    public static void until(Predicate<WebDriver> predicate) {
        getWebDriverWait().until(predicate);
    }

}

package com.wiley.qa.was.elements.questionlist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.stream.Stream;

public class WASAdminQuestionList extends TypifiedElement {

    private static final By QUESTION_LOCATOR = By.cssSelector("div.panel");

    public WASAdminQuestionList(WebElement wrappedElement) {
        super(wrappedElement);
    }

    public Stream<WASAdminQuestionItem> items() {
        return getWrappedElement().findElements(QUESTION_LOCATOR).stream().map(WASAdminQuestionItem::new);
    }
}

package com.wiley.qa.was.elements.questionlist;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

public class WASAdminQuestionItem extends TypifiedElement {
    private static final By QUESTION_NAME_LOCATOR = By.xpath(".//span[@data-selenium-id = 'question-name']");
    private static final By QUESTION_ID_LOCATOR = By.xpath(".//span[@data-selenium-id = 'question-id']");
    private static final By LAUNCH_QUESTION_LOCATOR = By.xpath(".//button[@data-selenium-id = 'launch-question-btn']");

    public WASAdminQuestionItem(WebElement wrappedElement) {
        super(wrappedElement);
    }

    public String getName() {
        return getWrappedElement().findElement(QUESTION_NAME_LOCATOR).getText();
    }

    public String getMasterQuestionId() {
        return getWrappedElement().findElement(QUESTION_ID_LOCATOR).getText();
    }

    public WebElement getLaunchButton() {
        return getWrappedElement().findElement(LAUNCH_QUESTION_LOCATOR);
    }

    public void launch() {
        getLaunchButton().click();
    }

    @Override
    public String toString() {
        return "WASAdminQuestionItem{Name='" + getName() + "', MasterQuestionId='" + getMasterQuestionId() + "'}";
    }
}

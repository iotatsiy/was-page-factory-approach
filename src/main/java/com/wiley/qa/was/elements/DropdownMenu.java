package com.wiley.qa.was.elements;

import com.wiley.qa.was.util.PageFactoryExtention;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import java.util.*;
import java.util.stream.Stream;

public class DropdownMenu extends TypifiedElement {

    private static final By EXPAND_BUTTON_LOCATOR = By.xpath(".//BUTTON");
    private static final By OPTION_LOCATOR = By.xpath(".//UL/LI/A");

    public DropdownMenu(WebElement wrappedElement) {
        super(wrappedElement);
    }

    public void choose(String option) {
        expand();
        selectOptions(Collections.singletonList(option));
        unexpand();
    }

    public void choose(List<String> options) {
        expand();
        selectOptions(options);
        unexpand();
    }

    private void selectOptions(List<String> options) {
        options().filter(x -> options.contains(x.getText()))
                .findFirst()
                .ifPresent(WebElement::click);
    }

    public void unexpand() {
        PageFactoryExtention.until(x -> {
            button().click();
            return !isOpened();
        });
    }

    public void expand() {
        button().click();
        PageFactoryExtention.until(x -> !isEmpty());
        if (!isOpened()) {
            button().click();
            PageFactoryExtention.until(x -> isOpened());
        }
    }

    public WebElement button() {
        return getWrappedElement().findElement(EXPAND_BUTTON_LOCATOR);
    }

    public boolean isEmpty() {
        return options().count() == 0;
    }

    public boolean isOpened() {
        return getWrappedElement().getAttribute("class").contains("open");
    }

    public Stream<WebElement> options() {
        return getWrappedElement().findElements(OPTION_LOCATOR).stream();
    }

}

package com.wiley.qa.was.pages.wasadmin;

import com.wiley.qa.was.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

public class WASAdminBasePage extends BasePage {

    @ElementTitle("Questions")
    @FindBy(xpath = "*//A[node() = 'Questions']")
    public Link questionsLink;

    @ElementTitle("Products")
    @FindBy(xpath = "*//A[node() = 'Products']")
    public Link productsLink;

    public WASAdminBasePage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
    }
}

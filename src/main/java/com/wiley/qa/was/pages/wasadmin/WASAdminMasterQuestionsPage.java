package com.wiley.qa.was.pages.wasadmin;

import com.wiley.qa.was.elements.DropdownMenu;
import com.wiley.qa.was.elements.questionlist.WASAdminQuestionList;
import com.wiley.qa.was.util.PageFactoryExtention;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

@PageEntry(title = "Administration tool - Master Questions")
public class WASAdminMasterQuestionsPage extends WASAdminBasePage {

    private static final Logger LOGGER = Logger.getLogger(WASAdminMasterQuestionsPage.class);

    @ElementTitle("Select Root Folder")
    @FindBy(xpath = "*//div[child::select[@title = 'Select Root Folder']]")
    public DropdownMenu selectRootFolderDropdownMenu;

    @ElementTitle("Select Question Type")
    @FindBy(xpath = "*//div[child::select[@title = 'Question type']]")
    public DropdownMenu selectQuestionTypeDropdownMenu;

    @ElementTitle("Select Question Assistance")
    @FindBy(xpath = "*//div[child::select[@title = 'Question assistance']]")
    public DropdownMenu selectQuestionAssistanceDropdownMenu;

    @ElementTitle("Search in question title")
    @FindBy(xpath = "*//INPUT[@data-selenium-id = 'search-input']")
    public TextInput searchInQuestionTitleTextInput;

    @ElementTitle("Search")
    @FindBy(xpath = "*//BUTTON[@data-selenium-id = 'filter-apply-btn']")
    public Button searchButton;

    @ElementTitle("Question List")
    @FindBy(xpath = "*//div[contains(@class, 'question-list')]")
    public WASAdminQuestionList questionList;

    @ElementTitle("Load Indicator")
    @FindBy(css = "p.load-indicator")
    public WebElement loadIndicator;

    @ElementTitle("No questions found message")
    @FindBy(xpath = "*//DIV[contains(text(), 'No questions found.')]")
    public WebElement noQuestionsFound;

    public WASAdminMasterQuestionsPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
    }

    @ActionTitle("wait for load questions")
    public void waitForLoadQuestion() {
        LOGGER.info("Waiting for load questions...");
        PageFactoryExtention.until(x -> questionList.items().count() != 0 || noQuestionsFound.isDisplayed());
        LOGGER.info("Questions: " + questionList.items().count());
    }
}

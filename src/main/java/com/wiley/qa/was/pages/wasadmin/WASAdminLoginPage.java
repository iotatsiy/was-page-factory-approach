package com.wiley.qa.was.pages.wasadmin;

import com.wiley.qa.was.pages.BasePage;
import org.openqa.selenium.support.FindBy;
import ru.sbtqa.tag.pagefactory.PageFactory;
import ru.sbtqa.tag.pagefactory.annotations.ActionTitle;
import ru.sbtqa.tag.pagefactory.annotations.ElementTitle;
import ru.sbtqa.tag.pagefactory.annotations.PageEntry;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementLocatorFactory;

@PageEntry(title = "Administration tool - Login page")
public class WASAdminLoginPage extends BasePage {

    @FindBy(xpath = "*//INPUT[@id = 'email-address']")
    @ElementTitle("Email address")
    public TextInput emailAddressInput;

    @FindBy(xpath = "*//INPUT[@id = 'password']")
    @ElementTitle("Password")
    public TextInput passwordInput;

    @FindBy(xpath = "*//BUTTON[node() = 'Log in']")
    @ElementTitle("Log in")
    public Button loginButton;

    public WASAdminLoginPage() {
        PageFactory.initElements(new HtmlElementDecorator(new HtmlElementLocatorFactory(PageFactory.getDriver())), this);
    }

    @ActionTitle("log in as")
    public void login(String email, String password) {
        emailAddressInput.sendKeys(email);
        passwordInput.sendKeys(password);
        loginButton.click();
    }

}

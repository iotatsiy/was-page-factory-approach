package com.wiley.qa.was.steps;

import cucumber.api.java.en.And;
import org.apache.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.HashMap;
import java.util.Map;

public class ComputingDef {
    private static final Logger LOG = Logger.getLogger(ComputingDef.class);
    private static final ScriptEngine ENGINE;
    private static final Map<String, String> CACHE;

    static {
        ScriptEngineManager manager = new ScriptEngineManager();
        ENGINE = manager.getEngineByName("javascript");
        CACHE = new HashMap<>();
    }


    @And("^compute \"([^\"]*)\" and save as \"([^\"]*)\"$")
    public void checkCssAttribute(String expression, String key) throws ScriptException {
        String prepared = prepareExpression(expression);
        String result = ENGINE.eval(prepared).toString();
        LOG.info("COMPUTING " + prepared + " = " + result + " [" + expression + "]");
        LOG.info("SAVING [" + key + "] " + getCache().get(key) + " -> " + result);
        getCache().put(key, result);
    }

    public static Map<String, String> getCache() {
        return CACHE;
    }

    public static String prepareExpression(String source) {
        String result = source;
        for (Map.Entry<String, String> entry : getCache().entrySet()) {
            result = result.replaceAll("#" + entry.getKey() + "#", entry.getValue());
        }
        return result;
    }
}

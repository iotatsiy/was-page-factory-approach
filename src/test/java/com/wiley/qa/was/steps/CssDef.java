package com.wiley.qa.was.steps;

import cucumber.api.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;

public class CssDef extends StepsDefinitionHelper {

    @And("^ASSERT CSS attribute of \"([^\"]*)\" with name \"([^\"]*)\" equals \"([^\"]*)\"$")
    public void checkCssAttribute(String title, String attributeName, String attributeValue) throws PageException {
        checkCssAttributeWithExtension(title, null, attributeName, attributeValue);
    }

    /**
     * @param title          Name of an element on the current page.
     * @param extension      Specific locator applying from basic element.
     * @param attributeName  Name of CSS attribute.
     * @param attributeValue Expected value of CSS attribute.
     * @throws PageException
     */
    @And("^ASSERT CSS attribute of \"([^\"]*)\"\\(([^\"]*)\\) with name \"([^\"]*)\" equals \"([^\"]*)\"$")
    public void checkCssAttributeWithExtension(String title, String extension, String attributeName, String attributeValue) throws PageException {
        WebElement element = getElementByTitle(title);
        if (extension != null && !extension.isEmpty()) {
            element = element.findElement(By.xpath(extension));
        }
        String actual = element.getCssValue(attributeName);
        attributeValue = ComputingDef.prepareExpression(attributeValue);
        Assert.assertTrue("Css attribute '" + attributeName + "' not equals '" + attributeValue + "'. " +
                "Actual value: '" + actual + "'", actual.equalsIgnoreCase(attributeValue));
    }
}

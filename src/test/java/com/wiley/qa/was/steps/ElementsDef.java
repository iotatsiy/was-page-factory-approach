package com.wiley.qa.was.steps;

import com.wiley.qa.was.elements.DropdownMenu;
import cucumber.api.java.en.And;
import ru.sbtqa.tag.pagefactory.exceptions.PageException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ElementsDef extends StepsDefinitionHelper {

    @And("^user is using dropdown menu \"([^\"]*)\" to choose \"([^\"]*)\"$")
    public void userActionOneParam(String title, String options) throws PageException {
        DropdownMenu dropdownMenu = getElementByTitle(title);
        List<String> list = Arrays.stream(options.split(",")).map(String::trim).collect(Collectors.toList());
        dropdownMenu.choose(list);
    }

}

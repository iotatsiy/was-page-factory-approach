﻿Feature: Test

  Background: Open the administration tool
    When user is on the page "Administration tool - Login page"
    * user $log in as "test@wiley.com" "password"

  Scenario: Check CSS[font-size] for "No questions found message"
    * user is on the page "Administration tool - Master Questions"
    * user is using dropdown menu "Select Root Folder" to choose "Kimmel_Financial_Accounting_7e_Canada"
    * user is using dropdown menu "Select Question Assistance" to choose "GO Tutorial"
    * user $click on the element by title "Search"
    * user $wait for load questions
    * ASSERT CSS attribute of "No questions found message" with name "font-size" equals "187px"
